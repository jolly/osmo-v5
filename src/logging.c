
#include <osmocom/core/utils.h>
#include "logging.h"

static const struct log_info_cat log_categories[] = {
	[DV5] = {
		.name = "DV5",
		.description = "V5 protocol",
		.color = "\033[0;31m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
	[DV5L1] = {
		.name = "DV5L1",
		.description = "V5 L1 link FSM",
		.color = "\033[0;36m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
	[DV5CTRL] = {
		.name = "DV5CTRL",
		.description = "V5 control protocol",
		.color = "\033[1;36m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
	[DV5PORT] = {
		.name = "DV5PORT",
		.description = "V5 port control protocol",
		.color = "\033[1;33m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
	[DV5PSTN] = {
		.name = "DV5PSTN",
		.description = "V5 PSTN protocol",
		.color = "\033[0;37m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
	[DV5LCP] = {
		.name = "DV5LCP",
		.description = "V5 LCP protocol",
		.color = "\033[0;33m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
	[DV5BCC] = {
		.name = "DV5BCC",
		.description = "V5 BCC protocol",
		.color = "\033[1;32m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
	[DV5PP] = {
		.name = "DV5PP",
		.description = "V5 Protection protocol",
		.color = "\033[0;35m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
	[DV5MGMT] = {
		.name = "DV5MGMT",
		.description = "V5 management",
		.color = "\033[1;37m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
	[DV5EF] = {
		.name = "DV5EF",
		.description = "V5 envelop frame",
		.color = "\033[1;31m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
	[DPH] = {
		.name = "DPH",
		.description = "PH-socket",
		.color = "\033[0;33m",
		.enabled = 1, .loglevel = LOGL_INFO,
	},
};

const struct log_info log_info = {
	.cat = log_categories,
	.num_cat = ARRAY_SIZE(log_categories),
};
