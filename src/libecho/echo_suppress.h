
typedef struct iir_filter {
	float a0, a1, a2, b1, b2;
	float z1, z2;
} iir_filter_t;

enum sup_state {
	SUP_STATE_SILENCE,
	SUP_STATE_SUPPRESSION,
	SUP_STATE_BREAK_IN,
};

typedef struct echo_sup_state {
	float		suppress_threshold;	/* linear value of suppression th */
	float		release_suppress;	/* linear value of release suppression th */
	float		insertion_loss;		/* linear value of insertion loss */
	int		suppress_operate;	/* number of sample until transition */
	int		suppress_hangover;
	int		break_in_operate;
	int		break_in_hangover;
	enum sup_state	state;			/* current state */
	int		timer;			/* timer to delay transition */
	iir_filter_t	lp_send, lp_receive;	/* filter to cut off high frequencies */
	iir_filter_t	hp_send, hp_receive;	/* filter to cut off low frequencies */
	iir_filter_t	env_send, env_receive;	/* filter to get the envelope after rectifying */
	iir_filter_t	dt_receive;		/* filter to detect steady tone (dial tone) */
	int		dt_timer, dt_interval;	/* timer of dial tone detection interval */
	float		dt_level, dt_offset;	/* initial level and max offset during interval */
	bool		dt_disable;		/* true if suppressor is disabled, due to dial tone */
} echo_sup_state_t;

echo_sup_state_t *echo_sup_create(void *ctx, int samplerate);
void echo_sup_free(echo_sup_state_t *es);
int16_t echo_sup_update(echo_sup_state_t *es, int16_t tx, int16_t rx);
void echo_sup_flush(echo_sup_state_t *es);
