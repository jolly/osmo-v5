
struct osmo_fsm_inst *v52_le_lcp_create(void *ctx, struct v5x_link *v5l, uint8_t id);
void v52_le_lcp_destroy(struct osmo_fsm_inst *fi);
bool v52_le_lcp_is_operational(struct osmo_fsm_inst *fi);
const char *v52_le_lcp_state_name(struct osmo_fsm_inst *fi);
void v52_le_lcp_init(void);
int v52_le_lcp_mph_rcv(struct v5x_link *v5l, enum v5x_mph_prim prim);
int v52_le_lcp_fe_rcv(struct v5x_link *v5l, enum v52_link_ctrl_func lcf);
int v52_le_lcp_mdu_snd(struct v5x_link *v5l, enum v5x_mgmt_prim prim);
