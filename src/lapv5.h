#pragma once

#include <osmocom/abis/lapd.h>

struct lapv5_instance {
	struct llist_head list;		/* list of LAPV5 instances */
	bool network_side;

	int (*ph_data_req_cb)(struct msgb *msg, void *cbdata);
	void *ph_data_req_cbdata;
	int (*dl_receive_cb)(struct osmo_dlsap_prim *odp, uint16_t dladdr, void *rx_cbdata);
	void *dl_receive_cbdata;

	struct lapd_profile profile;	/* must be a copy */

	struct llist_head sap_list;	/* list of SAP/datalinks in this instance */
	int pcap_fd;			/* PCAP file descriptor */
	char *name;			/* human-readable name */

	bool enabled;			/* only receive messages when enabled */
};

extern const struct lapd_profile lapd_profile_lapv5dl;

int lapv5ef_rx(struct v5x_link *link, struct msgb *msg);

int lapv5_dl_est_req(struct lapv5_instance *li, uint16_t dladdr);
int lapv5_dl_rel_req(struct lapv5_instance *li, uint16_t dladdr);
int lapv5_dl_data_req(struct lapv5_instance *li, uint16_t dladdr, struct msgb *msg);

int lapv5_ph_data_ind(struct lapv5_instance *li, struct msgb *msg, int *error);

struct lapv5_instance *lapv5_instance_alloc(int network_side,
	int (*ph_data_req_cb)(struct msgb *msg, void *cbdata), void *ph_data_req_cbdata,
	int (*dl_receive_cb)(struct osmo_dlsap_prim *odp, uint16_t dladdr, void *rx_cbdata), void *dl_receive_cbdata,
	const struct lapd_profile *profile, const char *name);
void lapv5_instance_set_profile(struct lapv5_instance *li, const struct lapd_profile *profile);
void lapv5_instance_free(struct lapv5_instance *li);

int lapv5ef_rx(struct v5x_link *link, struct msgb *msg);
int lapv5ef_tx(struct v5x_user_port *v5up, struct msgb *msg);

void lapv5_set_enabled(struct lapv5_instance *li, bool enabled);
