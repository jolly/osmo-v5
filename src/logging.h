#pragma once

#include <osmocom/core/logging.h>

enum {
	DV5,
	DV5L1,
	DV5CTRL,
	DV5PORT,
	DV5PSTN,
	DV5LCP,
	DV5BCC,
	DV5PP,
	DV5MGMT,
	DV5EF,
	DPH,
};

extern const struct log_info log_info;
