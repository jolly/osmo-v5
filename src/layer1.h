
enum l1_signal_prim {
	L1_SIGNAL_LOS,
	L1_SIGNAL_NO_LOS,
	L1_SIGNAL_RAI,
	L1_SIGNAL_NO_RAI,
	L1_SIGNAL_AIS,
	L1_SIGNAL_NO_AIS,
	L1_SIGNAL_SA7_1,
	L1_SIGNAL_SA7_0,
};

int v5x_l1_signal_snd(struct v5x_link *v5l, enum l1_signal_prim prim);
int ph_activate_req(struct v5x_timeslot *ts);
int ph_deactivate_req(struct v5x_timeslot *ts);
int ph_data_req_hdlc(struct msgb *msg, struct v5x_interface *v5if);
int ph_data_req_dl_cc(struct msgb *msg, void *cbdata);
int ph_data_req_dl_prot(struct msgb *msg, void *cbdata);
void ph_socket_rx_cb(ph_socket_t *s, int channel, uint8_t prim, uint8_t *data, int length);
int gsmtap_init(void);
int l1_signal_init(void);
struct e1inp_line *e1_line_init(struct v5x_link *v5l, int e1_nr);
void e1_line_exit(struct v5x_link *v5l);
