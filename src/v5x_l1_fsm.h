
void v5x_l1_mph_snd(struct v5x_link *v5i, enum v5x_mph_prim prim);
struct v5x_l1_proto *v5x_l1_fsm_create(void *ctx, struct v5x_link *v5l, uint8_t id);
void v5x_l1_fsm_destroy(struct v5x_l1_proto *l1);
void v5x_l1_init(void);
bool v5x_l1_is_up(struct v5x_l1_proto *l1);
int v5x_l1_signal_rcv(struct v5x_link *v5i, enum l1_signal_prim prim);
int v5x_l1_signal_snd(struct v5x_link *v5i, enum l1_signal_prim prim);
