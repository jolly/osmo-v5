
const char *v5x_le_system_fsm_state_name(struct osmo_fsm_inst *fi);
const char *v5x_le_pstn_dl_fsm_state_name(struct osmo_fsm_inst *fi);
const char *v5x_le_pstn_rs_fsm_state_name(struct osmo_fsm_inst *fi);
const char *v5x_le_unblk_all_fsm_state_name(struct osmo_fsm_inst *fi);
void v5x_le_mgmt_init(void);
struct v5x_mgmt_proto *v5x_le_mgmt_create(struct v5x_interface *v5if);
void v5x_le_mgmt_destroy(struct v5x_mgmt_proto *mgmt);
void v5x_le_mgmt_start_delay(struct v5x_interface *v5if, int delay);
int v5x_le_mgmt_start(struct v5x_interface *v5if);
int v5x_le_pstn_restart(struct v5x_interface *v5if);
int v5x_le_align_ports(struct v5x_interface *v5if, int accelerated);
void v5x_le_mdl_rcv(struct v5x_interface *v5if, struct v5x_link *v5l, enum v5x_mgmt_prim prim, uint16_t dladdr);
void v5x_le_common_ctrl_rcv(struct v5x_interface *v5if, uint8_t cfi, uint8_t variant, uint8_t rej_cause,
	uint32_t interface_id);
void v5x_le_provisioning_start(struct v5x_interface *v5if);
void v5x_le_mgmt_port_unblock(struct v5x_user_port *v5up);
void v5x_le_mgmt_port_block(struct v5x_user_port *v5up);
void v5x_le_nat_ph_rcv(struct v5x_user_port *v5up, uint8_t prim, uint8_t *data, int length);
void v5x_le_port_mph_rcv(struct v5x_user_port *v5up, enum v5x_mph_prim prim, uint8_t perf_grading);
void v5x_le_nat_fe_rcv(struct v5x_user_port *v5up, struct msgb *msg);
void v5x_le_pstn_fe_rcv(struct v5x_user_port *v5up, enum v5x_fe_prim prim, struct msgb *msg);
void v5x_le_pstn_mdu_rcv(struct v5x_user_port *v5up, enum v5x_mgmt_prim prim);
void v52_le_lcp_mdu_rcv(struct v5x_link *v5l, enum v5x_mgmt_prim prim);
int v52_le_pp_mdu_rcv(struct v5x_interface *v5if, enum v5x_mgmt_prim prim, uint8_t link_id, uint8_t ts,
	const uint8_t *cause, uint8_t cause_len);
void v5x_le_channel_assign(struct v5x_user_port *v5up, int channel);
void v5x_le_channel_unassign(struct v5x_user_port *v5up, int channel);
int v52_le_bcc_mdu_rcv(struct v5x_interface *v5if, uint8_t link_id, uint8_t ts,
	uint8_t __attribute__((unused)) *isdn_multislot, enum v5x_mgmt_prim prim, const uint8_t *cause,
	uint8_t cause_len);
