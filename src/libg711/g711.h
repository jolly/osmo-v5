void g711_init(void);

/* Xlaw -> signed 16-bit */
extern int16_t *g711_ulaw_flipped_to_linear;
extern int16_t *g711_alaw_flipped_to_linear;
extern int16_t *g711_alaw_to_linear;
extern int16_t *g711_ulaw_to_linear;

/* signed 16-bit -> Xlaw */
extern uint8_t *g711_linear_to_alaw_flipped;
extern uint8_t *g711_linear_to_ulaw_flipped;
extern uint8_t *g711_linear_to_alaw;
extern uint8_t *g711_linear_to_ulaw;
