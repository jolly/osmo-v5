
struct v52_pp_proto *v52_le_pp_create(struct v5x_interface *v5if);
void v52_le_pp_destroy(struct v52_pp_proto *pp);
void v52_le_pp_init(void);
int v52_le_pp_dl_rcv(struct v5x_link *v5l, uint16_t cc_id, uint8_t msg_type, const struct tlv_parsed *tp);
int v52_le_pp_mdu_snd(struct v5x_interface *v5if, enum v5x_mgmt_prim prim, uint8_t link_id, uint8_t ts, uint8_t cause);
