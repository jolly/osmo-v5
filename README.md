osmo-v5 - Osmocom implementation of ETSI/ITU V5 interface
=========================================================

This is a repository containing *very early work-in-progress* code implementing
the ETSI/ITU V5 interface as specified in ITU-T G.964 + G.965.

V5 is an interface spoken between digital telephone exchange and its [remote]
access network (multiplexers + concentrators of subscriber access lines).

For some general intro into V5, you might find the following OsmoDevCall
recording interesting: https://media.ccc.de/v/osmodevcall-20211228-laforge-retro-isdn-v5

Initial development was done just using the spec as reference; from 09/2022 onwards
a Nokia EKSOS N20 became the primary target.

Homepage
--------

None yet.

GIT Repository
--------------

You can clone from the official libosmocore.git repository using

        git clone https://gitea.osmocom.org/laforge/osmo-v5

There is a web interface at <https://gitea.osmocom.org/laforge/osmo-v5>

Mailing List
------------

Discussions related to osmo-v5 are happening on the
discussion@lists.retronetworking.org mailing list, please see
<https://lists.retronetworking.org/postorius/lists/discussion.lists.retronetworking.org/> for subscription
options and the list archive.

Please observe the [Osmocom Mailing List
Rules](https://osmocom.org/projects/cellular-infrastructure/wiki/Mailing_List_Rules)
when posting.
